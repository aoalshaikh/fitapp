import { Component, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs//add/operator/map';
import { Nav, Platform, AlertController, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Health } from '@ionic-native/health';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ChartsPage } from '../pages/charts/charts';

import { HealthDataService } from '../providers/health-data-service/health-data-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    private health: Health,
    private healthService: HealthDataService,
    private alertCtrl: AlertController,
    private http: HttpClient,
    private toastCtrl: ToastController,
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage },
      { title: 'Charts', component: ChartsPage }
    ];

  }

  ionViewDidLoad() {
    
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
  
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
  
    toast.present();
  }

  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'data',
      subTitle: JSON.stringify(msg),
      buttons: ['Dismiss']
    });
    alert.present();
  }

  installFitAlert() {
    let alert = this.alertCtrl.create({
      title: 'Google Fit غير موجود على الجهاز',
      subTitle: 'ينصح بتثبيت تطبيق Google Fit',
      buttons: ['إغلاق', {
        text: 'تثبيت',
        handler: data => {
          this.health.promptInstallFit()
            .then(response => {
              this.presentAlert(JSON.stringify(response));
            })
            .catch(error => {
              this.presentAlert(JSON.stringify(error));
            });
        }
      }]
    });
    alert.present();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.health.isAvailable()
        .then(response => {
          if(!response) {
            this.installFitAlert()
          }
        });
      this.authorize();
    });
  }

  authorize() {
    this.healthService.getPermission()
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        console.log(error);
      });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
