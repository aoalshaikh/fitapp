import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ChartsModule } from 'ng2-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ChartsPage } from '../pages/charts/charts';

//pipes
import { KeysPipe } from '../pipes/key-values.pipe';

//directives
import { AbsoluteDragDirective } from "../directives/absolute-drag.directive";

// providers
import { HealthDataService } from '../providers/health-data-service/health-data-service';

// ionic native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Health } from '@ionic-native/health';
import { Pedometer } from '@ionic-native/pedometer';

//plugins
import {NgxChartsModule} from '@swimlane/ngx-charts';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    ChartsPage,
    KeysPipe,
    AbsoluteDragDirective
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    ChartsModule,
    BrowserAnimationsModule,
    NgxChartsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    ChartsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Health,
    HealthDataService,
    Pedometer,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
