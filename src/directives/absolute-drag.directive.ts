import { Directive, Input, ElementRef, Renderer, OnInit, OnDestroy } from '@angular/core';
import { DomController } from 'ionic-angular';
import { Gesture } from 'ionic-angular/gestures/gesture';
declare var Hammer: any;

@Directive({
  selector: '[swipe-vertical]'
})
export class AbsoluteDragDirective implements OnInit, OnDestroy {

  private swipeGesture: Gesture;
  private swipeDownGesture: Gesture;
  private el: HTMLElement;

  constructor(
    public element: ElementRef,
    public renderer: Renderer,
    public domCtrl: DomController,

  ) {
    this.el = element.nativeElement;
  }

  ngOnInit() {
    this.swipeGesture = new Gesture(this.el, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_VERTICAL}]
      ]
    });

    this.swipeGesture.listen();
    this.swipeGesture.on('swipeup', e => {
      this.element.nativeElement.style.top = '-400px';
    });
    this.swipeGesture.on('swipedown', e => {
      this.element.nativeElement.style.top = '10px';
    })
  }

  ngOnDestroy() {
    this.swipeGesture.destroy()
  }

  // ngAfterViewInit() {
  //
  //   let hammer = new window['Hammer'](this.element.nativeElement);
  //   hammer.get('pan').set({ directive: window['Hammer'].DIRECTION_VERTICAL });
  //
  //   hammer.on('pan', (ev) => {
  //     this.handlePan(ev);
  //   });
  // }
  //
  // handlePan(ev) {
  //   let newLeft = ev.center.x;
  //   let newTop = ev.center.y;
  //
  //   newTop >= 200? newTop = 200 : newTop = -150;
  //
  //   this.domCtrl.write(() => {
  //     // this.renderer.setElementStyle(this.element.nativeElement, 'left', newLeft + 'px');
  //     this.renderer.setElementStyle(this.element.nativeElement, 'top', newTop + 'px');
  //   });
  // }

}
