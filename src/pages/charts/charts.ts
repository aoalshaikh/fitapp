import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { HealthDataService } from '../../providers/health-data-service/health-data-service';
import { Pedometer } from '@ionic-native/pedometer';
import {NgxChartsModule} from '@swimlane/ngx-charts';

/**
 * Generated class for the ChartsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

// @IonicPage({
//   name: 'ChartsPage'
// })

@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html',
})
export class ChartsPage {
  loading: boolean = true;
  pedoStepsAvailable: boolean = false;
  pedoDistanceAvailable: boolean = false;
  steps: any = 0;
  distance: any = 0;
  liveData: any = {};
  error: any = '';

  currentActivity: any = '';
  activities: any = [];
  activitNames = [
    'Walking',
    'Running',
    'Biking',
    'Boxing',
  ];

  seconds: number = 0;
  timerGoing: boolean = false;

  single: any[] = [
    {
      "name": "Germany",
      "value": 200
    },
    {
      "name": "USA",
      "value": 700
    },
    {
      "name": "France",
      "value": 1200
    }
  ];
  multi: any[] = [
    {
      "name": "Germany",
      "series": [
        {
          "name": "2010",
          "value": 7300000
        },
        {
          "name": "2011",
          "value": 8940000
        }
      ]
    },

    {
      "name": "USA",
      "series": [
        {
          "name": "2010",
          "value": 7870000
        },
        {
          "name": "2011",
          "value": 8270000
        }
      ]
    },

    {
      "name": "France",
      "series": [
        {
          "name": "2010",
          "value": 5000002
        },
        {
          "name": "2011",
          "value": 5800000
        }
      ]
    }
  ];

  view: any[] = [700, 400];

  colorScheme = {
    domain: ['#999999']
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private healthService: HealthDataService,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private pedometer: Pedometer
  ) {
  }

  isPedoAvailable() {
    this.pedometer.isStepCountingAvailable()
      .then(response => {
        this.pedoStepsAvailable = response;
      })
      .catch(error => {
        this.pedoStepsAvailable = error;
      });

    this.pedometer.isDistanceAvailable()
      .then(response => {
        this.pedoDistanceAvailable = response;
      })
      .catch(error => {
        this.pedoDistanceAvailable = error;
      });
  }

  liveDataInit() {
    this.seconds = 0;
    this.pedometer.startPedometerUpdates()
      .subscribe(data => {
        // this.presentAlert(JSON.stringify(data));
        this.liveData = data;
        this.steps = data.numberOfSteps;
      },
      error => {
        this.presentAlert(JSON.stringify(error));
        this.error = error;
      });
  }

  stopData() {
    this.startTimer(false);
    this.pedometer.stopPedometerUpdates()
      .then(data => {
        // this.presentAlert(JSON.stringify(data));
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error));
      });

    if(this.steps > 0 && this.seconds > 0) {
      let time = this.sToTime(this.seconds);
      this.activities.push({activity: this.currentActivity, steps: this.steps, time: time});
    }
  }

  startTimer(going: boolean = true) {
    this.timerGoing = going;
    this.timerInit();
  }

  timerInit() {
    let timer = setInterval(() => {
      this.timerGoing? this.seconds++ : clearInterval(timer);
    }, 1000);
  }

  resetTimer() {
    this.seconds = 0;
    this.startTimer(false);
  }

  sToTime(duration) {
    let seconds: any = +duration%60;
    let minutes: any = +(duration/60);
    let hours: any = +(duration/60/60);

    hours = (Math.floor(hours) < 10) ? "0" + Math.floor(hours) : Math.floor(hours);
    minutes = (Math.floor(minutes) < 10) ? "0" + Math.floor(minutes) : Math.floor(minutes);
    seconds = (Math.floor(seconds) < 10) ? "0" + Math.floor(seconds) : Math.floor(seconds);

    return hours + ":" + minutes + ":" + seconds;
  }

  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'data',
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  goToHome() {
    this.navCtrl.push('home');
  }

  onSelect(event) {
    console.log(event);
  }
}
