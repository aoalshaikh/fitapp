import { Component, ElementRef, Renderer, ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IonicPage, NavController, AlertController, ToastController, DomController } from 'ionic-angular';
import { Health } from '@ionic-native/health';
import { HealthDataService } from '../../providers/health-data-service/health-data-service';
import { Pedometer, IPedometerData } from '@ionic-native/pedometer';
import {NgxChartsModule} from '@swimlane/ngx-charts';

// @IonicPage({
//   name: 'HomePage'
// })
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  token = 'Bearer ya29.GlsDBSNY-7ZbhC25zFT3tbMSDm4qqH4J3zT645ICpch_mtiZg7Qpoo18i7KQ4A_vDC5nQgFv4jpLxfAu2ZrGK8nGMi62oXu7jrF7FxETVgkOn6iZTor6SaT8H48c';
  isAuth: any = false;
  mySteps: any = [];
  myGender: any = [];
  myHeight: any = [];
  myWeight: any = [];
  myActivity: any = [];
  myHeartRate: any = [];
  myBirthDay: any = [];
  myNutrition: any = [];
  myCalories: any[] = [];
  myCaloriesActive: any = [];
  myCaloriesBasal: any = [];
  myDistance: any = [];

  myTop: number = 20;

  test: any = {};

  @ViewChild('draggable') draggable: ElementRef;

  activities = [
    'Walking',
    'Running',
    'Biking',
    'Boxing',
  ];

  pedometerData: any = {};
  ispedo: any;

  public event = {
    startDate: new Date().getMonth() + '/' + new Date().getDate() + '/' + new Date().getFullYear(),
    startTime: new Date().getHours() + ':' + new Date().getMinutes() + ' ',
    hours: undefined,
    minutes: undefined,
    seconds: undefined,
    endDate: new Date(),
    dataType: 'activity',
    steps: 0,
    calories: 0,
    value: 0,
    sourceName: 'lava',
    sourceBundleId: 'io.ionic.starter'
  };

  single: any[] = [
    {
      "name": "Germany",
      "value": 200
    },
    {
      "name": "USA",
      "value": 700
    },
    {
      "name": "France",
      "value": 1200
    }
  ];

  view: any[] = [700, 400];

  colorScheme = {
    domain: ['#FFFFFF', '#783429', '#fa7382']
  };

  constructor(
    public navCtrl: NavController,
    private health: Health,
    private alertCtrl: AlertController,
    private http: HttpClient,
    private toastCtrl: ToastController,
    private healthService: HealthDataService,
    private pedometer: Pedometer,

    public element: ElementRef,
    public renderer: Renderer,
    public domCtrl: DomController
  ) { }

  ionViewDidLoad() {
  }

  pedo() {
    this.pedometer.isStepCountingAvailable()
      .then(data => {
        this.ispedo = data;
      })
      .catch(error => {
        this.presentAlert(error);
      });
  }

  getSteps() {
    this.healthService.getSteps()
      .then(data => {
        this.presentAlert(data);
        this.mySteps = data;
      })
      .catch(error => {

      })
  }

  setSteps(steps: any = 200) {
    this.event.value = steps;
    this.healthService.storeSteps(this.event)
      .then(response => {
        this.presentAlert(JSON.stringify(response));
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error));
      })
  }

  getCalories() {
    this.healthService.getCalories(new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000))
      .then(data => {
        // this.myCalories = Object.keys(data);
        for(var p in data) {
          this.myCalories.push({ 'name': data[p].startDate.getDate(), 'value': Math.floor(data[p].value) });
        }
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error))
      });
  }

  isArray(array) {
    return Array.isArray(array);
  }

  getCaloriesActive() {
    this.healthService.getCaloriesActive(new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000))
      .then(data => {
        this.myCaloriesActive = data;
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error))
      });
  }

  getCaloriesBasal() {
    this.healthService.getCaloriesBasal(new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000))
      .then(data => {
        this.myCaloriesBasal = data;
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error))
      });
  }

  setCalories() {
    let nutritionData = {
      startDate:  new Date(new Date().getTime() - 9 * 60 * 60 * 1000), // three hours ago
      endDate: new Date(new Date().getTime() - 4 * 60 * 60 * 1000),
      dataType: 'calories',
      value: 8000,
      sourceName: 'lava',
      sourceBundleId: 'io.ionic.starter'
    }
    this.healthService.setCalories(nutritionData)
      .then(response => {
        this.presentAlert(JSON.stringify(response));
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error));
      });

  }

  getActivity() {
    this.getCalories();
    this.getCaloriesActive();
    this.getCaloriesBasal();
    this.healthService.getActivity(new Date(new Date().getTime() - 3 * 24 * 60 * 60 * 1000))
      .then(data => {
        this.myActivity = data;
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error))
      });
  }

  setActivity() {
    let activityData = {
      startDate: new Date(this.event.startDate + ', ' + this.event.startTime),
      endDate: new Date(new Date(this.event.startDate + ', ' + this.event.startTime).getTime() + ( ( (+this.event.hours * 60 * 60) + (+this.event.minutes * 60 + (+this.event.seconds) )) * 1000 )),
      dataType: 'activity',
      value: this.event.value,
      calories: this.event.calories,
      steps: this.event.steps,
      sourceName: 'lava',
      sourceBundleId: 'io.ionic.starter'
    }

    this.healthService.setActivity(activityData)
      .then(response => {
        this.presentAlert(JSON.stringify(response));
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error));
      });
  }

  msToTime(duration) {
    let milliseconds: any = +((duration%1000)/100);
    let seconds: any = +((duration/1000)%60);
    let minutes: any = +(duration/(1000*60)%60);
    let hours: any = +(duration/(1000*60*60)%24);

    hours = (Math.floor(hours) < 10) ? "0" + Math.floor(hours) : Math.floor(hours);
    minutes = (Math.floor(minutes) < 10) ? "0" + Math.floor(minutes) : Math.floor(minutes);
    seconds = (Math.floor(seconds) < 10) ? "0" + Math.floor(seconds) : Math.floor(seconds);

    return hours + ":" + minutes + ":" + seconds;
}

  getDistance() {
    this.healthService.getDistance()
      .then(data => {
        this.myDistance = data;
      })
      .catch(error => {
        this.presentAlert(JSON.stringify(error));
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  presentAlert(msg) {
    let alert = this.alertCtrl.create({
      title: 'data',
      subTitle: msg,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  onSelect(event: any) {
    console.log(event);
  }

}
