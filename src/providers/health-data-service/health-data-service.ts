import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs//add/operator/map';
import { ToastController } from 'ionic-angular';
import { Health } from '@ionic-native/health';

@Injectable()
export class HealthDataService {

    constructor (
        private health: Health,
        private toastCtrl: ToastController,
    ) {}

    promptInsatll() {
        this.health.promptInstallFit()
        .then(response => {
            this.presentToast(response);
        })
        .catch(error => {
            this.presentToast(error);
        });
    }

    getPermission() {
        return new Promise((resolve, reject) => {
            this.health.isAvailable()
            .then((available:boolean) => {
              console.log(available);
              // this.presentAlert(available);
              this.health.requestAuthorization([
                'steps',
                'height',
                'weight', 
                'gender',
                'activity', 
                'date_of_birth', 
                'nutrition', 
                'heart_rate', 
                'calories', 
                'distance', 
                'fat_percentage',  //read and write permissions
              ])
              .then(authorized => {
                resolve(authorized);
              })
              .catch(authorizationError => {
                  reject(authorizationError);
              });
            })
            .catch(noFoundError => {
            });
        });
    }
    presentToast(msg) {
        let toast = this.toastCtrl.create({
          message: msg,
          duration: 3000,
          position: 'top'
        });
      
        toast.onDidDismiss(() => {
          console.log('Dismissed toast');
        });
      
        toast.present();
    }

    getActivity(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'activity'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    setActivity(activityData: any) {
        return new Promise((resolve, reject) => {
            this.health.store(activityData)
                .then(successResponse => {
                    resolve(successResponse);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }

    getSteps(startDate: Date = new Date(new Date().setHours(0,0,0,0)), endDate: Date = new Date(), bucket: string = 'week') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'steps',
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    storeSteps(stepsData: any) {
        
        return new Promise((resolve, reject) => {
            this.health.store(stepsData)
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            })
        })
    }

    getCalories(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                // filtered: true,
                dataType: 'calories'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    setCalories(caloriesData: any) {
        return new Promise((resolve, reject) => {
            this.health.store(caloriesData)
                .then(successResponse => {
                    resolve(successResponse);
                })
                .catch(error => {
                    reject(error);
                });
        })
    }

    getCaloriesActive(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'calories.active'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getCaloriesBasal(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'calories.basal'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getDistance(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'distance'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getHeight(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'height'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getWeight(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'weight'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getHeartRate(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'heart_rate'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getFatPercentage(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'fat_percentage'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getBloodGlucose(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'blood_glucose'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutrition(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'nutrition'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getNutritionX(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date(), bucket: string = 'day') {
        return new Promise((resolve, reject) => {
            this.health.queryAggregated({
                startDate: startDate,
                endDate: endDate,
                bucket: bucket,
                dataType: 'nutrition.x'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getNutritionCalories(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.calories'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getNutritionFatTotal(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.total'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

    getNutritionFatSaturated(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.saturated'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionFatUnsaturated(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.unsaturated'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionFatPolyunsaturated(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.polyunsaturated'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionFatMonounsaturated(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.monounsaturated'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionFatTrans(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.fat.trans'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionCholesterol(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.cholesterol'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionSodium(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.sodium'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionPotassium(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.potassium'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionCarbsTotal(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.carbs.total'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionDietaryFiber(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.dietary_fiber'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionSugar(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.sugar'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionProtein(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.protein'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionVitaminA(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.vitamin_a'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionVitaminC(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.vitamin_C'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionCalcium(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.calcium'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionIron(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.iron'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionWater(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.water'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }
    
    getNutritionCaffeine(startDate: Date = new Date(new Date().getTime() - 1000 * 60 * 60 * 24), endDate: Date = new Date()) {
        return new Promise((resolve, reject) => {
            this.health.query({
                startDate: startDate,
                endDate: endDate,
                dataType: 'nutrition.caffeine'
            })
            .then(successResponse => {
                resolve(successResponse);
            })
            .catch(error => {
                reject(error);
            });
        });
    }

}